﻿#include <iostream>
#include <math.h>

class MyClass
{
public:
    MyClass() : a(10)
    {}
    int GetA()
    {
        return a;
    }

    void SetA(int newA)
    {
        a = newA;
    }

private:
    int a = 0;
};

class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(float _x, float _y, float _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z;
    }

    float length()
    {
        return sqrt(x * x + y * y + z * z);
    }

private:
    float x;
    float y;
    float z;

};

int main()
{
    MyClass test;
    Vector Vtest(4,4,4);
    std::cout << test.GetA() << std::endl;
    test.SetA(20);
    std::cout << test.GetA() << std::endl;

    Vtest.Show();
    std::cout << '\n' << Vtest.length();

    return 0;
}